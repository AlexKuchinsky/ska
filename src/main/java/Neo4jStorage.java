import org.neo4j.driver.v1.*;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Transaction;
import org.neo4j.driver.v1.TransactionWork;

import static org.neo4j.driver.v1.Values.parameters;

public class Neo4jStorage {
    private Driver driver;

    public Neo4jStorage( String uri, String user, String password )
    {
        driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password));
    }


    public void addNode(final String label, final String name, final String value)
    {
        Session session = driver.session();
        String result = session.writeTransaction( new TransactionWork<String>()
        {
            public String execute( Transaction tx )
            {
                StatementResult result = tx.run( "CREATE (a) SET a.value = $value, a.name = $name, a:"+label,
                        parameters( "value", value, "name", name));
                return result.single().get( 0 ).asString();
            }
        } );

//        String greeting = session.writeTransaction( new TransactionWork<String>()
//        {
//            public String execute( Transaction tx )
//            {
//                StatementResult result = tx.run( "CREATE (a:Greeting) " +
//                                "SET a.message = $message " +
//                                "RETURN a.message + ', from node ' + id(a)",
//                        parameters( "message", label));
//                return result.single().get( 0 ).asString();
//            }
//        } );
        //System.out.println( greeting );

        System.out.println("Transaction result: "+result);
        driver.close();
    }
}
