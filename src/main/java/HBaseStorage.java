import org.apache.commons.math3.util.Pair;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.HColumnDescriptor;
import org.apache.hadoop.hbase.HTableDescriptor;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class HBaseStorage {
    private String tableName;
    private HTable table;
    private Configuration config;
    
    public HBaseStorage(String tableName){
        this.tableName = tableName;
        config = HBaseConfiguration.create();
        try{
            table = new HTable(config, tableName);
        }catch (IOException e){
            System.out.println(e);
        }

    }

    public void createTabe(List<String> families){
        try{
            HBaseAdmin admin = new HBaseAdmin(config);
            HTableDescriptor tableDescriptor = new HTableDescriptor(TableName.valueOf(tableName));
            for (String f: families) {
                tableDescriptor.addFamily(new HColumnDescriptor(f));
            }
            admin.createTable(tableDescriptor);
        }catch (Exception e){
            System.out.println(e);
        }

    }

    public void insertData(String rowKey, HashMap<String, List<Pair<String, String>>> data){
        try{
            Put p = new Put(Bytes.toBytes(rowKey));
            for(Map.Entry<String, List<Pair<String, String>>> entry: data.entrySet()){
                String family = entry.getKey();
                List<Pair<String, String>> columnsValues = entry.getValue();
                for (Pair<String, String> columnValue: columnsValues) {
                    String column = columnValue.getFirst();
                    String val = columnValue.getSecond();
                    p.addImmutable(Bytes.toBytes(family), Bytes.toBytes(column), Bytes.toBytes(val));
                }

            }
            table.put(p);
            table.close();
        }catch (Exception e){
            System.out.println(e);
        }

    }
    public String getData(String rowKey, String family, String column){
        try{
            Get g = new Get(Bytes.toBytes(rowKey));
            Result r = table.get(g);
            String cell = Bytes.toString(r.getValue(Bytes.toBytes(family),Bytes.toBytes(column)));
            return cell;
        }catch (IOException e){
            System.out.println(e);
        }
        return null;
    }
}
